Flaky Tools
==============

flaky-tools contains 3 key components:
* @Flaky(jiraIssue = "EX-341", expiryDate = "2017-12-30")
* IgnoreFlakes implements TestRule
* FlakeyTransform implements Consumer<FlakeyTransform.Params>

At first @Flaky is identical to junit's @Ignore, and made to work with the IgnoreFlakes test rule (place it early in your rule chain). Aside from adding an issue key for tracking there are two important differences: If the property "run.flaky.on" is set ONLY the annotated tests will run, and that after the expiryDate passes all flaky tests will fail, demanding that they be fixed or deleted. As we all know, flaky tests are worse than no test.

The last piece, the FlakeyTransform, makes use of the spoon AST transformer to add @Flaky to the source file. Give it the Class/Method/src dir where the flaky behavior occurs and it will add the Flaky annotation for you.  

Have a look for test re-runners and the bitbucket api and you'll have all the tools you need to completely automate test quarantine in your CI build. 

Usage
======

```java
public class MyTestClass {
    @Rule
    public RuleChain chain = RuleChain.emptyRuleChain()
            .around(new IgnoreFlakes());

    @Test
    @Flaky(jiraIssue = "FAKE-1234", expiryDate = "2025-01-01")
    public void FailsExpiredTests() {
    }
}
```

You'll need to configure failsafe to rerun tests

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-failsafe-plugin</artifactId>

    <configuration>
        <rerunFailingTestsCount>3</rerunFailingTestsCount>
    </configuration>
</plugin>
```

Then observe those tests with a Listener installed in failsafe
```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-failsafe-plugin</artifactId>
    <configuration>
        <properties>
            <property>
                <name>listener</name>
                <value>com.example.JUnitFlakyTestListener</value>
            </property>
        </properties>
    </configuration>    
  </plugin>
```

If you're listener sees the same test pass and fail you've got yourself a flake and its time to invoke the FlakyTransform.


Installation
============

In your maven pom.xml:

```xml 
<dependency>
    <groupId>com.atlassian</groupId>
    <artifactId>flaky-tools</artifactId>
    <version>0.1</version>
</dependency>
```

Tests
=====

```bash
mvn test
```

Contributors
============

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests
* Aspire to one commit per PR (`git rebase -i` is your friend)

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

License
========

Copyright (c) 2017 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.