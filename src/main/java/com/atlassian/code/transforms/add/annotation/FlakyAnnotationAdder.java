package com.atlassian.code.transforms.add.annotation;

import spoon.processing.AbstractProcessor;
import spoon.reflect.declaration.CtAnnotation;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.reference.CtPackageReference;
import spoon.reflect.reference.CtTypeReference;

import java.lang.annotation.Annotation;

public class FlakyAnnotationAdder extends AbstractProcessor<CtMethod> {

    private final String className;
    private final String methodName;

    public FlakyAnnotationAdder(String className, String methodName) {
        this.className = className;
        this.methodName = methodName;
    }

    @Override
    public void process(CtMethod ctMethod) {
        if (methodName.equals(ctMethod.getSimpleName())
                && ctMethod.getParent((CtClass<?> c) -> className.equals(c.getQualifiedName())) != null) {


            CtPackageReference pkgRef = getFactory().Core().createPackageReference().setSimpleName("com.atlassian.test.rules");
            CtTypeReference<Annotation> flakyRef = getFactory().Core().createTypeReference();
            flakyRef.setSimpleName("Flaky");
            flakyRef.setPackage(pkgRef);

            CtAnnotation<? extends Annotation> flaky = getFactory().Code().createAnnotation(flakyRef);
            ctMethod.addAnnotation(flaky);
        }
    }

}
